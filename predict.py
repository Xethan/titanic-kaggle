import csv

def write_predictions_to_file(predictions):
	try:
		with open('predictions.csv', 'w+') as csvfile:
			field_names = ['PassengerId', 'Survived']
			csv_writer = csv.DictWriter(csvfile, fieldnames=field_names)
			csv_writer.writeheader()
			for i in range(predictions.shape[0]):
				csv_writer.writerow({'PassengerId': 892 + i, 'Survived': predictions[i]})
	except OSError as e:
		print(e)

def predict(model, x_test):
	predictions = model.predict(x_test)
	write_predictions_to_file(predictions)