import argparse
import numpy
import pandas

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

import engineer
import evaluate
from plot import plot
from train import train

def describe(dataFrame):
	print(dataFrame.describe())
	print(dataFrame.describe(include=[numpy.object]))

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument('--describe', action='store_true', dest='describe')
	parser.add_argument('--train', action='store_true', dest='train')
	parser.add_argument('--predict', action='store_true', dest='predict')
	parser.add_argument('--lc', '--learning_curves', action='store_true', dest='learning_curves')
	parser.add_argument('--rfc', action='store_true', dest='random_forest')

	parser.add_argument('--correlation', action='store_true', dest='correlation')
	parser.add_argument('-a', '--age', action='store_true', dest='age')
	parser.add_argument('-s', '--sex', action='store_true', dest='sex')
	parser.add_argument('-f', '--family', action='store_true', dest='family')
	parser.add_argument('-t', '--ticket', action='store_true', dest='ticket')
	parser.add_argument('-e', '--emabarkation_port', action='store_true', dest='embarkation_port')
	parser.add_argument('-p', '--price', '--fare', action='store_true', dest='fare')
	parser.add_argument('--title', action='store_true', dest='title')
	parser.add_argument('-c', '--pclass', action='store_true', dest='pclass')
	args = parser.parse_args()
	return args

args = parse_args()

def main():
	dataFrame = pandas.read_csv('train.csv', index_col=0)
	if args.describe:
		describe(dataFrame)
	dataFrame = engineer.base_features(dataFrame)
	if args.train:
		if args.random_forest:
			train(dataFrame, args, RandomForestClassifier, engineer.features_random_forest, evaluate.random_forest_classifier)
		else:
			train(dataFrame, args, LogisticRegression, engineer.features_logistic_regression, evaluate.logistic_regression)
	else:
		plot(dataFrame, args)

if __name__ == '__main__':
	main()