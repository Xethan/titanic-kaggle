import numpy
import pandas
import matplotlib.pyplot as pyplot
import matplotlib.mlab as mlab
from sklearn.utils import shuffle
from sklearn.model_selection import learning_curve
import seaborn

def plot_learning_curves(dataFrame, model):
	X, Y = shuffle(dataFrame.iloc[:, 1:], dataFrame['Survived'])
	train_sizes, train_scores, test_scores = learning_curve(model(), X, Y, train_sizes=mlab.frange(.1, 1, .1), cv=10)
	train_scores_mean = numpy.mean(train_scores, axis=1)
	train_scores_std = numpy.std(train_scores, axis=1)
	test_scores_mean = numpy.mean(test_scores, axis=1)
	test_scores_std = numpy.std(test_scores, axis=1)
	pyplot.grid()
	pyplot.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.1, color="r")
	pyplot.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")
	pyplot.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score")
	pyplot.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")
	pyplot.show()

def plot_fare_survival_rate(dataFrame):
	data = [ [x[8] for x in dataFrame.values if x[0] == did_survive and x[8]] for did_survive in [0, 1]]
	seaborn.distplot(data[0])
	seaborn.distplot(data[1])
	pyplot.show()

	seaborn.regplot(data=dataFrame, x='Fare', y='Survived')
	pyplot.show()

	dataFrame['Fare'] = dataFrame['Fare'].apply(lambda x: x - x % 10 if x < 100 else x - x % 100)
	seaborn.countplot(x='Fare', hue='Survived', data=dataFrame)
	pyplot.show()

def plot_survival_rate(dataFrame, feature_name):
	seaborn.countplot(x=feature_name, hue='Survived', data=dataFrame)
	pyplot.show()

def plot_correlations(dataFrame):
	correlation_dataFrame = dataFrame.pivot_table(index='Family category', columns='Ticket category', fill_value=0, aggfunc='size')
	category_order = ['Alone', 'Small', 'Big']
	correlation_dataFrame = correlation_dataFrame.reindex(category_order, axis=0)
	correlation_dataFrame = correlation_dataFrame.reindex(category_order, axis=1)
	seaborn.heatmap(correlation_dataFrame, annot=True, fmt='d', cbar=False)
	pyplot.show()

	correlation_dataFrame = dataFrame.pivot_table(index='Embarked', columns='Pclass', fill_value=0, aggfunc='size')
	seaborn.heatmap(correlation_dataFrame, annot=True, fmt='d', cbar=False)
	pyplot.show()

def plot(dataFrame, args):
	seaborn.set(style='darkgrid')
	if args.correlation:
		plot_correlations(dataFrame)
	if args.age:
		plot_survival_rate(dataFrame, 'Age category')
	if args.sex:
		plot_survival_rate(dataFrame, 'Sex')
	if args.family:
		plot_survival_rate(dataFrame, 'Family category')
	if args.ticket:
		plot_survival_rate(dataFrame, 'Ticket category')
	if args.embarkation_port:
		plot_survival_rate(dataFrame, 'Embarked')
	if args.title:
		plot_survival_rate(dataFrame, 'Title')
	if args.pclass:
		plot_survival_rate(dataFrame, 'Pclass')
	if args.fare:
		plot_fare_survival_rate(dataFrame)