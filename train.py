from plot import plot_learning_curves
import engineer
from predict import predict
import evaluate
import pandas
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

def normalize_data(x_train, x_test):
	scaler = StandardScaler()
	scaler.fit(x_train)
	x_train = pandas.DataFrame(scaler.transform(x_train), index=x_train.index, columns=x_train.columns)
	x_test = pandas.DataFrame(scaler.transform(x_test), index=x_test.index, columns=x_test.columns)
	return x_train, x_test

def prepare_XY(dataFrame, args, func_engineer_features_for_model):
	if args.predict == True:
		x_train, y_train = dataFrame.iloc[:, 1:], dataFrame['Survived']
		x_test = pandas.read_csv('test.csv', index_col=0)
		x_test = engineer.base_features(x_test)
		x_test = engineer.missing_features_test_set(x_test)
		x_test = func_engineer_features_for_model(x_test, is_train_set=False)
		return x_train, x_test, y_train, None
	else:
		return train_test_split(dataFrame.iloc[:, 1:], dataFrame['Survived']) # shuffles the data

def train(dataFrame, args, func_model, func_engineer_features_for_model, func_evaluate):
	dataFrame = engineer.missing_features_train_set(dataFrame)
	dataFrame = func_engineer_features_for_model(dataFrame, is_train_set=True)
	x_train, x_test, y_train, y_test = prepare_XY(dataFrame, args, func_engineer_features_for_model)
	x_train, x_test = normalize_data(x_train, x_test)
	model = func_model()
	model.fit(x_train, y_train)
	if args.predict == True:
		predict(model, x_test)
	else:
		func_evaluate(model, x_test, y_test)
		if args.learning_curves:
			plot_learning_curves(dataFrame, func_model)