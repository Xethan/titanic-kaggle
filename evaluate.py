import numpy

def random_forest_classifier(random_forest, x_test, y_test):
	argsort = numpy.argsort(random_forest.feature_importances_)
	print(x_test.columns.values[argsort][::-1])
	print(random_forest.feature_importances_[argsort][::-1])
	print(random_forest.score(x_test, y_test))

def logistic_regression(logistic_regression, x_test, y_test):
	print(x_test.columns.values)
	print(logistic_regression.coef_[0])
	print(logistic_regression.score(x_test, y_test))
	