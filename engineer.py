import pandas
import numpy
import re # regex

def get_size_category(x):
	if x != 1:
		return 'Small' if x <= 4 else 'Big'
	return 'Alone'

def get_age_category(x):
	if x < 15:
		return 'Age 0 - 14'
	elif x < 35:
		return 'Age 15 - 34'
	elif x < 65:
		return 'Age 35 - 64'
	else:
		return 'Age 64+'

def get_simplified_title(title):
	if title in ['Ms', 'Lady', 'Mlle', 'the Countess']:
		return 'Miss'
	elif title in ['Mme', 'Ms', 'Dona']:
		return 'Mrs'
	elif title in ['Sir', 'Don', 'Jonkheer']:
		return 'Mr'
	elif title in ['Rev', 'Dr', 'Major', 'Col', 'Capt']:
		return 'Officer'
	return title

def missing_features_test_set(x_test):
	#test = x_test[(x_test['Embarked'] == 'S').values & (x_test['Pclass'] == 3).values]
	#print(test['Fare'].describe())
	x_test.at[1044, 'Fare'] = 8.05
	return x_test

def missing_features_train_set(x_train):
	x_train.at[62, 'Embarked'] = 'C'
	x_train.at[830, 'Embarked'] = 'C'
	return x_train

def features_random_forest(dataFrame, is_train_set):
	if is_train_set == True:
		X = pandas.DataFrame(dataFrame['Survived'])
		X['Pclass'] = dataFrame['Pclass']
	else:
		X = pandas.DataFrame(dataFrame['Pclass'])
	X['Title'] = dataFrame['Title'].replace({'Mr': 0, 'Officer': 1, 'Master': 2, 'Miss': 3, 'Mrs' : 4})
	X['Sex'] = dataFrame['Sex'].replace({'male': 0, 'female': 1})
	X['Fare'] = dataFrame['Fare']
	X['Embarked'] = dataFrame['Embarked'].replace({'S': 0, 'Q': 1, 'C': 2})
	X['Ticket size'] = dataFrame['Ticket category'].replace({'Big': 0, 'Alone': 1, 'Small': 2})
	X['Family size'] = dataFrame['Family category'].replace({'Big': 0, 'Alone': 1, 'Small': 2})
	#X['SibSp'] = dataFrame['SibSp']
	#X['Parch'] = dataFrame['Parch']
	X['Age'] = dataFrame['Age category'].replace({'Age 64+': 0, 'Age 15 - 34': 1, 'Age 35 - 64': 2, 'Age 0 - 14': 3})
	#X = pandas.get_dummies(X, columns=['Pclass', 'Title', 'Ticket size', 'Age', 'Embarked', 'Family size'])
	return X

def features_logistic_regression(dataFrame, is_train_set):
	if is_train_set == True:
		X = pandas.DataFrame(dataFrame['Survived'])
		X['Pclass'] = dataFrame['Pclass']
	else:
		X = pandas.DataFrame(dataFrame['Pclass'])
	X['Title'] = dataFrame['Title'].replace({'Mr': 0, 'Officer': 1, 'Master': 2, 'Miss': 3, 'Mrs' : 4})
	X['Sex'] = dataFrame['Sex'].replace({'male': 0, 'female': 1})
	X['Fare'] = dataFrame['Fare']
	X['Embarked'] = dataFrame['Embarked'].replace({'S': 0, 'Q': 1, 'C': 2})
	X['Ticket size'] = dataFrame['Ticket category'].replace({'Big': 0, 'Alone': 1, 'Small': 2})
	X['Family size'] = dataFrame['Family category'].replace({'Big': 0, 'Alone': 1, 'Small': 2})
	X['SibSp'] = dataFrame['SibSp']
	X['Parch'] = dataFrame['Parch']
	X['Age'] = dataFrame['Age category'].replace({'Age 64+': 0, 'Age 15 - 34': 1, 'Age 35 - 64': 2, 'Age 0 - 14': 3})
	return X

def base_features(dataFrame):
	dataFrame['Family size'] = dataFrame['SibSp'] + dataFrame['Parch'] + 1
	dataFrame['Family category'] = dataFrame['Family size'].apply(get_size_category)
	dataFrame['Ticket size'] = dataFrame['Ticket'].apply(lambda x: numpy.sum(dataFrame['Ticket'].values == x))
	dataFrame['Ticket category'] = dataFrame['Ticket size'].apply(get_size_category)
	dataFrame['Age category'] = dataFrame['Age'].apply(get_age_category)
	dataFrame['Title'] = [get_simplified_title(re.split("[,.][ ]?", name, 2)[1]) for name in dataFrame['Name'].values]
	return dataFrame